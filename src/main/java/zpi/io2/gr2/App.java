package zpi.io2.gr2;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class App {

    private static final String STATES_FILE_NAME = "/data.txt";
    private static final String PRODUCTS_FILE_NAME = "/product_list.csv";

    public static void main(String[] args) throws IOException {
        DataAccess dataAccess = new TextFileDataAccess(STATES_FILE_NAME, PRODUCTS_FILE_NAME);
        List<State> states = dataAccess.getStates();
        List<Product> products = dataAccess.getProducts();
        UserInput userInput = new ConsoleInput();
        Product chosenProduct = null;

        displayProducts(products);
        while (chosenProduct == null) {
            chosenProduct = userInput.getProductFromUser(products);
        }
        displayProducts(List.of(chosenProduct));
        System.out.println("Podaj nowa cene zakupu (wcisnij Enter aby pominąć)");
        Double basePrice = userInput.getPriceFromUser();
        if (basePrice != null) {
            chosenProduct.setPrice(basePrice);
        }
        System.out.println("Podaj cene brutto (wcisnij Enter ab pominąć)");
        Double grossPrice = userInput.getPriceFromUser();
        if (grossPrice != null) {
            displayResult(states, chosenProduct, grossPrice);
        } else {
            displayResult(states, chosenProduct);
        }
    }

    private static void displayResult(List<State> states, Product product) {
        final String RESULT_FORMAT = "%-20s %-20s %-20s %-20s %-20s %-20s%n";
        NumberFormat formatter = new DecimalFormat("#0.00");
        System.out.printf(RESULT_FORMAT, "ID", "Stan", "Cena zakupu", "Cena netto", "Cena brutto", "Zysk");
        states.forEach(state -> {
            Double profit = Calculator.getProfit(product.getPrice());
            Double taxedPrice = Calculator.getPriceAfterTax(product.getPrice() + profit, state, product.getCategoryName());
            System.out.printf(RESULT_FORMAT, states.indexOf(state) + 1, state, product.getPrice(), Calculator.getNetPrice(product.getPrice()), formatter.format(taxedPrice), formatter.format(profit));
        });
    }

    private static void displayResult(List<State> states, Product product, Double grossPrice) {
        final String RESULT_FORMAT = "%-20s %-20s %-20s %-20s %-20s %-20s %-20s%n";
        NumberFormat formatter = new DecimalFormat("#0.00");
        System.out.printf(RESULT_FORMAT, "ID", "Stan", "Cena zakupu", "Cena netto", "Cena brutto", "Zysk", "");
        states.forEach(state -> {
            String negativeProfitAlert = "";
            Double tax = state.getTaxRateByCategory(product.getCategoryName());
            Double profit = Calculator.getProfit(product.getPrice(), Calculator.getNetPrice(grossPrice, tax));
            if (profit <= 0) {
                negativeProfitAlert = "BRAK ZYSKU";
            }
            System.out.printf(RESULT_FORMAT, states.indexOf(state) + 1, state, product.getPrice(), formatter.format(Calculator.getNetPrice(grossPrice, tax)), grossPrice, formatter.format(profit), negativeProfitAlert);
        });
    }

    private static void displayProducts(List<Product> collection) {
        final String PRODUCTS_FORMAT = "%-20s %-20s %-20s %-20s%n";
        System.out.printf(PRODUCTS_FORMAT, "ID", "Nazwa", "Kategoria", "Cena");
        collection.forEach(item -> System.out.printf(PRODUCTS_FORMAT, collection.indexOf(item) + 1, item.getName(), item.getCategoryName(), item.getPrice()));
    }

    private static void displayCollection(Map<CategoryName, Double> collection) {
        StringBuilder stringBuilder = new StringBuilder();
        AtomicInteger ordinal = new AtomicInteger(1);
        collection.forEach((categoryName, tax) -> {
            stringBuilder.append(ordinal.getAndIncrement());
            stringBuilder.append(": ");
            stringBuilder.append(categoryName);
            stringBuilder.append(" ");
            stringBuilder.append(tax * 100);
            stringBuilder.append("%");
            stringBuilder.append("\n");
        });
        System.out.print(stringBuilder);
    }
}
