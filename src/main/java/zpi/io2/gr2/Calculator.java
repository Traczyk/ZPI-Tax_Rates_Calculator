package zpi.io2.gr2;

public class Calculator {
    private static final Double MARGIN = 0.1;

    public static Double getPriceAfterTax(Double price, State state, CategoryName categoryName) {
        return state.getTaxRateByCategory(categoryName) * price + price;
    }

    public static Double getProfit(Double basePrice) {
        return basePrice * MARGIN;
    }

    public static Double getProfit(Double basePrice, Double netPrice) {
        return netPrice - basePrice;
    }

    public static Double getNetPrice(Double basePrice) {
        return basePrice + basePrice * MARGIN;
    }

    public static Double getNetPrice(Double grossPrice, Double tax) {
        return grossPrice / (1.0 + tax);
    }

    public static Double getGrossPrice(Double netPrice, Double tax) {
        return netPrice * tax + netPrice;
    }
}
