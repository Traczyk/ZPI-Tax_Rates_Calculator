package zpi.io2.gr2;


public enum CategoryName {
    GROCERIES("groceries"),
    PREPARED_FOOD("prepared food"),
    PRESCRIPTION_DRUG("prescription drug"),
    NON_PRESCRIPTION_DRUG("non-prescription drug"),
    CLOTHING("clothing"),
    INTANGIBLES("intangibles");

    private String name;

    CategoryName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}