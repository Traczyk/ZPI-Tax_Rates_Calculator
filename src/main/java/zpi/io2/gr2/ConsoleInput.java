package zpi.io2.gr2;

import java.util.*;

public class ConsoleInput implements UserInput {
    private static final char INCENTIVE_SIGN = '>';
    private Scanner scanner;

    public ConsoleInput() {
        this.scanner = new Scanner(System.in);
    }

    @Override
    public State getChosenState(List<State> states) {
        System.out.print(INCENTIVE_SIGN);
        try {
            int chosenId = Integer.parseInt(scanner.nextLine());
            if (chosenId < 1 || chosenId > states.size()) {
                System.err.println("Invalid state ID");
                return null;
            }
            return states.get(chosenId - 1);
        } catch (NumberFormatException e) {
            System.err.println("State ID must be numeric");
            return null;
        }
    }

    @Override
    public CategoryName getChosenCategory(List<State> states) {
        List<CategoryName> categoryNames = new ArrayList<>();
        categoryNames.addAll(Arrays.asList(CategoryName.values()));
        System.out.print(INCENTIVE_SIGN);
        try {
            int chosenId = Integer.parseInt(scanner.nextLine());
            if (chosenId < 1 || chosenId > categoryNames.size()) {
                System.err.println("Invalid category ID");
                return null;
            }
            return categoryNames.get(chosenId - 1);
        } catch (NumberFormatException e) {
            System.err.println("Category ID must be numeric");
            return null;
        }
    }

    @Override
    public Double getPriceFromUser() {
        System.out.print(INCENTIVE_SIGN);
        try {
            Double price = Double.parseDouble(scanner.nextLine());
            if(price <= 0) {
                return null;
            }
            return price;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public Product getProductFromUser(List<Product> products) {
        System.out.print(INCENTIVE_SIGN);
        try {
            int chosenId = Integer.parseInt(scanner.nextLine());
            if(chosenId < 1 || chosenId > products.size()) {
                System.err.println("Invalid product ID");
                return null;
            }
            return products.get(chosenId - 1);
        } catch(NumberFormatException e) {
            System.err.println("Product ID must be numeric");
            return null;
        }
    }
}
