package zpi.io2.gr2;

import java.io.IOException;
import java.util.List;

public interface DataAccess  {
    List<State> getStates() throws IOException;
    List<Product> getProducts() throws IOException;
}
