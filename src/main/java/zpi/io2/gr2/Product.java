package zpi.io2.gr2;

public class Product {
    private String name;
    private Double price;
    private CategoryName categoryName;

    public Product(String name, CategoryName categoryName, Double price) {
        this.name = name;
        this.price = price;
        this.categoryName = categoryName;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public CategoryName getCategoryName() {
        return categoryName;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", categoryName=" + categoryName +
                '}';
    }
}
