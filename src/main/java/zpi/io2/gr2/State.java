package zpi.io2.gr2;

import java.util.Map;

public class State {
    private String name;
    private Map<CategoryName, Double> taxesByCategories;

    public State(String name, Map<CategoryName, Double> taxesByCategories) {
        this.name = name;
        this.taxesByCategories = taxesByCategories;
    }

    public void setTaxRateForCategory(CategoryName name, Double taxRate) {
        taxesByCategories.put(name, taxRate);
        throw new RuntimeException("Unknown category name");
    }

    public Double getTaxRateByCategory(CategoryName name) {
        return taxesByCategories.get(name);
    }

    public String getName() {
        return name;
    }

    public Map<CategoryName, Double> getTaxesByCategories() {
        return taxesByCategories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;

        return name.equals(state.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }
}
