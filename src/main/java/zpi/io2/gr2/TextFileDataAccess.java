package zpi.io2.gr2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class TextFileDataAccess implements DataAccess {
    private final String STATES_FILE_NAME;
    private final String PRODUCTS_FILE_NAME;


    public TextFileDataAccess(String STATES_FILE_NAME, String PRODUCTS_FILE_NAME) {
        this.STATES_FILE_NAME = STATES_FILE_NAME;
        this.PRODUCTS_FILE_NAME = PRODUCTS_FILE_NAME;
    }

    @Override
    public List<State> getStates() throws IOException{
        List<State> states = new ArrayList<>();
        Set<CategoryName> categoriesNames = getCategoriesNames();
        Map<CategoryName, Double> taxesByCategory = new LinkedHashMap<>();

        InputStream inputStream = getClass().getResourceAsStream(STATES_FILE_NAME);
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String data;
            int rowNumber = 1;
            int categoryId = 1;
            while ((data = bufferedReader.readLine()) != null) {
                if (rowNumber == 1) {
                    rowNumber++;
                    continue;
                }
                String[] rowElements = data.split(";");

                for (CategoryName categoryName : categoriesNames) {
                    taxesByCategory.put(categoryName, Double.parseDouble(rowElements[categoryId]));
                    categoryId++;
                }
                states.add(new State(rowElements[0], new LinkedHashMap<>(taxesByCategory)));
                categoryId = 1;
            }
        }
        return states;
    }

    public List<Product> getProducts() throws IOException{
        List<Product> products = new ArrayList<>();
        InputStream inputStream = getClass().getResourceAsStream(PRODUCTS_FILE_NAME);
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String data;
            while((data = bufferedReader.readLine()) != null) {
                String[] rowElements = data.split(",");
                CategoryName categoryName = null;
                for(CategoryName tempCategoryName : CategoryName.values()) {
                    if(rowElements[1].equals(tempCategoryName.toString())) {
                        categoryName = tempCategoryName;
                    }
                }
                products.add(new Product(rowElements[0], categoryName, Double.parseDouble(rowElements[2])));
            }
        }
        return products;
    }

    private Set<CategoryName> getCategoriesNames() {
        Set<CategoryName> categoriesNames = new LinkedHashSet<>();
        categoriesNames.addAll(Arrays.asList(CategoryName.values()));
        return categoriesNames;
    }
}
