package zpi.io2.gr2;

import java.util.List;

public interface UserInput {
    State getChosenState(List<State> states);

    CategoryName getChosenCategory(List<State> states);
    Double getPriceFromUser();
    Product getProductFromUser(List<Product> products);
}
