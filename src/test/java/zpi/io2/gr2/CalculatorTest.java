package zpi.io2.gr2;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class CalculatorTest {

    private static final String STATES_DB = "/data.txt";
    private static final String PRODUCTS_FILE_NAME = "/product_list.csv";
    private static final Double MARGIN = 0.1;
    private static final DataAccess dataAccess = new TextFileDataAccess(STATES_DB, PRODUCTS_FILE_NAME);
    private static List<State> states ;
    private Double price = 3.0;
    private CategoryName name = CategoryName.GROCERIES;

    @Test
    public void getPriceAfterTax() throws IOException {
        states = dataAccess.getStates();
        final Double profitAfterTax = 0.36;
        Double profit = states.get(0).getTaxRateByCategory(name) * price * price;
        assertTrue("Price after Tax is wrong", profit.equals(profitAfterTax));
    }

    @Test
    public void getProfit() {
        final Double profitWithMargin = 0.3;
        Double profit = price*MARGIN;
        profit = profit*100;
        profit = (double)Math.round(profit);
        profit = profit /100;
        assertTrue("Profit is wrong", profit.equals(profitWithMargin));
    }

    @Test
    public void getProfit1() throws IOException{
        states = dataAccess.getStates();
        Double tax = states.get(0).getTaxRateByCategory(name);
        Double grossPrice = 3.3;
        Double priceWithBaseProfit = (price + (price*MARGIN));
        Double finalProfit = - 0.132;
        Double profit = grossPrice- priceWithBaseProfit - (priceWithBaseProfit * tax);
        assertTrue("Profit is wrong", profit.equals(finalProfit));
    }
}