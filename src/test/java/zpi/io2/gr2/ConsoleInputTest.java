package zpi.io2.gr2;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

public class ConsoleInputTest {
    private static final String STATES_DB = "/data.txt";
    private static final String PRODUCTS_FILE_NAME = "/product_list.csv";

    private static final DataAccess dataAccess = new TextFileDataAccess(STATES_DB, PRODUCTS_FILE_NAME);
    private static List<State> states;

    private void createInputStreamFromString(String data) {
        InputStream in = new ByteArrayInputStream(data.getBytes());
        System.setIn(in);
    }

    private PipedInputStream captureSystemErrorStream() throws IOException {
        PipedOutputStream pipeOut = new PipedOutputStream();
        PipedInputStream pipeIn = new PipedInputStream(pipeOut);
        System.setErr(new PrintStream(pipeOut));
        return pipeIn;
    }

    @BeforeClass
    public static void readStatesFromDatabase() {
        try {
            states = dataAccess.getStates();
        } catch (IOException e) {
            fail("Unable to read database");
        }
    }

    @After
    public void restoreDefaultStreams() {
        System.setIn(System.in);
        System.setErr(System.err);
        System.setOut(System.out);
    }

    @Test
    public void testCorrectChosenState() {
        Integer stateId = states.size();
        createInputStreamFromString(String.valueOf(stateId));

        UserInput userInput = new ConsoleInput();
        State actualState = userInput.getChosenState(states);
        State expectedState = states.get(stateId - 1);

        assertEquals(expectedState, actualState);
    }

    @Test
    public void testWrongChosenState() throws IOException {
        Integer stateId = states.size() + 1;
        createInputStreamFromString(String.valueOf(stateId));
        PipedInputStream errorStream = captureSystemErrorStream();

        UserInput userInput = new ConsoleInput();
        State actualState = null;
        try {
            actualState = userInput.getChosenState(states);
        } catch (Throwable throwable) {
            fail("Should not throw exception");
        }

        assertEquals("Should return null for non-existing id", null, actualState);

        Scanner errorScanner = new Scanner(errorStream);
        assertTrue("Should print error message to System.err", errorScanner.hasNext());
    }

    @Test
    public void testNonNumericState() throws InputMismatchException, IOException {
        createInputStreamFromString("non-numeric");
        PipedInputStream errorStream = captureSystemErrorStream();

        UserInput userInput = new ConsoleInput();
        State actualState = null;
        try {
            actualState = userInput.getChosenState(states);
        } catch (Throwable throwable) {
            fail("Should not throw exception");
        }

        assertEquals("Should return null for non-numeric input", null, actualState);

        Scanner errorScanner = new Scanner(errorStream);
        assertTrue("Should print error message to System.err", errorScanner.hasNext());
    }

    @Test
    public void testNonNullProducts() throws NullPointerException, IOException{
        List<Product> products = dataAccess.getProducts();
        products.forEach(m ->
                assertTrue("\"Product must not contain null fields \"",(m.getName() != null) && (m.getPrice() != null) && (m.getCategoryName() != null))
        );
    }

    @Test
    public void testNonNegativePrice() throws IOException{
        List<Product> products = dataAccess.getProducts();
        products.forEach(m ->
                assertTrue("Price field must not contain negative value ", m.getPrice() > 0));
    }



}